#!/usr/bin/env bash

# Exit on errors
set -e

# Set sane umask
umask 022

# Set language (this helps sometimes)
export LANG=C

# Set a basic PATH
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

# Initial variables
NAME=yajl		# Program name
PNAME=$NAME		# Package name
VERSION=2.1.0		# Program version
PVERSION=$VERSION	# Package version (remove dashes if present)
ARCH=${ARCH:-x86_64}	# Package architecture
BUILD=${BUILD:-1rg}	# Build number plus packager initials
SRCDIR=$NAME-$VERSION   # Source directory name (usually $NAME-$VERSION)
SRCTAR=$NAME-${VERSION}.tar.gz # Source tarball name

# Check everything is properly set before starting
if [ -z "$NAME" -o -z "$PNAME" -o -z "$VERSION" -o -z "$PVERSION" -o -z "$SRCDIR" -o -z "$SRCTAR" ]; then
	echo "Missing initial parameters" 1>&2
	exit 1
fi

CWD=$( pwd )
TMP=${TMP:-/tmp}	# Location to compile the source
PKG=$TMP/package-$NAME	# Location to build the package (use "package-$NAME" to avoid poss. conflicts)

# Define compiler flags based on intended architecture
if [ "$ARCH" = "i486" ]; then
	SLKCFLAGS="-O2 -march=i486 -mtune=i686"
elif [ "$ARCH" = "i686" ]; then
	SLKCFLAGS="-O2 -march=i686 -mtune=i686"
elif [ "$ARCH" = "s390" ]; then
	SLKCFLAGS="-O2"
elif [ "$ARCH" = "x86_64" ]; then
	SLKCFLAGS="-O2"
elif [ "$ARCH" = "noarch" ]; then
	SLKCFLAGS=""
else
	echo "Unknown architecture" 1>&2 ; exit 1
fi

if [ "$ARCH" = "x86_64" ]; then
	SLKLIBDIR="/usr/lib64"
else
	SLKLIBDIR="/usr/lib"
fi

# Clear sandbox directory
rm -rf $PKG
mkdir -p $PKG

# Extract tarball
cd $TMP
tar xvf $CWD/$SRCTAR
cd $SRCDIR

# Correct general permissions/ownership
chown -R 0:0 .
find . -print0 | xargs -0r chmod a+Xr,go-w,u+w

# Apply patches if present
if ls $CWD/patches/*.diff &>/dev/null; then
	for patch in $CWD/patches/*.diff; do
		patch -p1 <$patch
	done
fi

# Build and installation steps, usually ./configure, make, make install
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=/usr -DLIB_SUFFIX=64 ..
make
make install DESTDIR=$PKG
cd ..

# Strip binaries and libs (this is broken for files containing ':' or '\n')
(
cd $PKG
find . | xargs file | grep "executable" | grep ELF | cut -f 1 -d : | xargs -r strip --strip-unneeded 2> /dev/null
find . | xargs file | grep "shared object" | grep ELF | cut -f 1 -d : | xargs -r strip --strip-unneeded 2> /dev/null
)

# Compress man pages
find $PKG -type f -path '*/man/*' -print0 | xargs -0r gzip --best
find $PKG -type l -path '*/man/*' -print0 | while read -d '' symlink; do
	ln -s "$( readlink "$symlink" ).gz" "${symlink}.gz"
	rm "$symlink"
done

# Create package docs
mkdir -p $PKG/usr/doc/$PNAME-$PVERSION
cat $CWD/$PNAME.slackbuild >$PKG/usr/doc/$PNAME-$PVERSION/$PNAME.slackbuild
cp -a ChangeLog [A-Z][A-Z][A-Z]* $PKG/usr/doc/$PNAME-$PVERSION

# Add package description
mkdir -p $PKG/install
cat >$PKG/install/slack-desc <<EOF
      |-----handy-ruler------------------------------------------------------|
$PNAME: $PNAME (Yet Another Json Library)
$PNAME:
$PNAME: http://lloyd.github.com/yajl/
$PNAME:
EOF

# Add install script
cat >$PKG/install/doinst.sh <<EOF
EOF

# Add desktop file (usually it does not exist or is not needed)
if [ -e $CWD/$PNAME.desktop ]; then
	mkdir -p $PKG/usr/share/applications
	cat $CWD/$PNAME.desktop > $PKG/usr/share/applications/$PNAME.desktop
fi

# Create package
cd $PKG
makepkg -l y -c n $TMP/$PNAME-$PVERSION-$ARCH-$BUILD.tgz

# Clean up the compilation and package directories
rm -rf $PKG
cd $TMP
rm -rf $SRCDIR
