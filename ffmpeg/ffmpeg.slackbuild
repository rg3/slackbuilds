#!/usr/bin/env bash

# Exit on errors
set -e

# Set sane umask
umask 022

# Set language (this helps sometimes)
export LANG=C

# Set a basic PATH
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

# Initial variables
PNAME=ffmpeg		# Package name
PVERSION=2.5.4		# Package version
VERSION=n$PVERSION	# Source version
ARCH=${ARCH:-x86_64}	# Package architecture
BUILD=${BUILD:-1rg}	# Build number plus packager initials

# Check everything is properly set before starting
if [ -z "$PNAME" -o -z "$PVERSION"  ]; then
	echo "Missing initial parameters" 1>&2
	exit 1
fi

CWD=$( pwd )
TMP=${TMP:-/tmp}	# Location to compile the source
PKG=$TMP/package-$PNAME	# Location to build the package (use "package-$PNAME" to avoid poss. conflicts)

if [ "$ARCH" = "x86_64" ]; then
	SLKLIBDIR="/usr/lib64"
else
	SLKLIBDIR="/usr/lib"
fi

# Clear sandbox directory
rm -rf $PKG
mkdir -p $PKG

# Extract tarball
( cd ffmpeg && git archive --format=tar --prefix=ffmpeg-$PVERSION/ "$VERSION" ) | ( cd $TMP && tar xvf - )
cd $TMP/ffmpeg-$PVERSION

# Correct general permissions/ownership
chown -R 0:0 .
find . -print0 | xargs -0r chmod a+Xr,go-w,u+w

# Apply patches if present
if ls $CWD/patches/*.diff &>/dev/null; then
	for patch in $CWD/patches/*.diff; do
		patch -p1 <$patch
	done
fi

# Build and installation steps, usually ./configure, make, make install
./configure \
	--prefix=/usr \
	--libdir=$SLKLIBDIR \
	--shlibdir=$SLKLIBDIR \
	--mandir=/usr/man \
	--enable-gpl \
	--enable-nonfree \
	--disable-doc \
	--enable-postproc \
	--enable-libmp3lame \
	--enable-libx264 \
	--enable-shared \
	--enable-openssl
make $MAKEFLAGS
make install DESTDIR=$PKG

# Strip binaries and libs (this is broken for files containing ':' or '\n')
(
cd $PKG
find . | xargs file | grep "executable" | grep ELF | cut -f 1 -d : | xargs -r strip --strip-unneeded 2> /dev/null
find . | xargs file | grep "shared object" | grep ELF | cut -f 1 -d : | xargs -r strip --strip-unneeded 2> /dev/null
)

# Compress man pages
find $PKG -type f -path '*/man/*' -print0 | xargs -0r gzip --best
find $PKG -type l -path '*/man/*' -print0 | while read -d '' symlink; do
	ln -s "$( readlink "$symlink" ).gz" "${symlink}.gz"
	rm "$symlink"
done

# Create package docs
mkdir -p $PKG/usr/doc/$PNAME-$PVERSION
cat $CWD/$PNAME.slackbuild >$PKG/usr/doc/$PNAME-$PVERSION/$PNAME.slackbuild
cp -a Changelog [A-Z][A-Z][A-Z]* $PKG/usr/doc/$PNAME-$PVERSION

# Add package description
mkdir -p $PKG/install
cat >$PKG/install/slack-desc <<EOF
      |-----handy-ruler------------------------------------------------------|
$PNAME: $PNAME (Multimedia Tool)
$PNAME:
$PNAME: FFmpeg is a complete, cross-platform solution to record, convert and
$PNAME: stream audio and video. It includes libavcodec, the leading
$PNAME: audio/video codec library.
$PNAME:
$PNAME: http://ffmpeg.org/
$PNAME:
EOF

# Add install script
cat >$PKG/install/doinst.sh <<EOF
EOF

# Add desktop file (usually it does not exist or is not needed)
if [ -e $CWD/$PNAME.desktop ]; then
	mkdir -p $PKG/usr/share/applications
	cat $CWD/$PNAME.desktop > $PKG/usr/share/applications/$PNAME.desktop
fi

# Create package
cd $PKG
makepkg -l y -c n $TMP/$PNAME-$PVERSION-$ARCH-$BUILD.tgz

# Clean up the compilation and package directories
rm -rf $PKG
cd $TMP
rm -rf ffmpeg-$PVERSION
